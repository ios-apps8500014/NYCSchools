# NYC Schools List

## Table of Contents
1. [Summary](#summary)
  - [School List API](#school-list-api)
  - [School SAT Score](#school-sat-score)
2. [Architectural Approach](#architectural-approach)
3. [School List View](#school-list-view)
4. [School Description & SAT Score](#school-description--sat-score)
5. [Unit tests](#unit-tests)
6. [If Given More Time, What would I change?](#if-given-more-time-what-would-i-change)

## Summary
Purpose of this coding challenge is the following:
1. Display a list of NYC schools via a REST API
2. Display the SAT Scores (if it exists) of that NYC School via a REST API

Before jumping into coding, I made sure to fully understand the requirements and provided REST API.

<br />

### School List API
By calling the [NYC Schools List API](https://dev.socrata.com/foundry/data.cityofnewyork.us/s3k6-pzi2) without any SoQL Queries, it displayed all NYC schools without any ordering mechanism. 

Hence here are some reasons to use SoQL Queries:
- List of NYC Schools via the UI will have some form of organization to the user instead of being a random list of schools
- Rather than request all NYC schools at once, it would be best to use **pagination** as user scrolls through list of schools

With that said, I ended up using the following SoQL Queries:
```
# Page through NYC schools by borough
# Order schools by school name in alphabetical order (ascending)

BASE_NYC_DATA_URL/s3k6-pzi2.json?boro=K&$order=school_name
```

<br />

### School SAT Score
As with the School List API, directly calling the [SAT Score API](https://dev.socrata.com/foundry/data.cityofnewyork.us/f9bf-2cp4) with SoQL Queries will be a nightmare because it returns SAT Scores for **ALL** NYC schools. Hence, I only fetch the SAT Score for a specific NYC school using that school's unique ID or **dbn** value.
```
Request URL:
BASE_NYC_DATA_URL/f9bf-2cp4.json?dbn=19K683

Response:
[
    {
        "dbn": "19K683",
        "school_name": "THE SCHOOL FOR CLASSICS: AN ACADEMY OF THINKERS, WRITERS AND PERFORMERS",
        "num_of_sat_test_takers": "38",
        "sat_critical_reading_avg_score": "383",
        "sat_math_avg_score": "400",
        "sat_writing_avg_score": "374"
    }
]
```

<br />

## Architectural Approach
After figuring out how to page through the REST API to display information in a friendly manner, I can begin to plan out the architecture of this app. 

I decided to go with **MVVM (Model, View, View Model)** architecture with separate modules for the following reasons:
- Ideal separation of concerns between modules.
- The UI will focus on simply displaying data
- The View Model will handle the logic and facilitate communication between the View and API module
- Since the View Model will be the source of the logic, I can make use of Swift `protocols` to abstract them and create unit tests.

For the **Model**, I'll use the `Codable` protocol in order to decode the API response into Swift specified models seamlessly.

Taking performance into account too, I'll be doing the following to reduce network calls:
- Perform a network call to fetch schools from borough
    - Network call is only performed once for each borough
- Perform a network call to fetch SAT Score for school
    - Network call is only performed once. The SAT Score is stored within `School` object's `satScore`.
    - Hence if user views the same school, no network call is performed and SAT score is fetched from object

<br />

## School List View
The `SchoolListController` will utilize a `UITableView` to display NYC schools by each borough using sections. As the user scrolls down and gets close to viewing all schools within that borough, I fetch schools for the next borough. 

Each cell will display the following:
- School name
- Address
- Grades supported (6-12 or 9-12)

Here's a screen video below in which NYC schools are listed, using boroughs as sections. The boroughs are listed alphabetically too, starting from Bronx and ending in Staten Island.

To checkout the screen video, click on the **SchoolListView.mp4** video found in this repo.

<br />

## School Description & SAT Score
Rather than utilize Apple's `UIKit`, I created the `SATScoreView` using `SwiftUI`. 

The view displays the following:
- The school's summary overview
- Conditionally display school's SAT Score
  - If SAT Score was never fetched, a network call is made then result is stored within School object
  - If SAT Score was already fetched, simply fetch already existing data from School object without performing another network call

Why conditionally? Because some schools from the SAT Score API: 
- Do not have SAT Scores at all
- Have SAT scores, but the scores have a value **S** instead of numerical

Here's an example of edge case 2:
```
[
    {
        "dbn": "08X376",
        "school_name": "ANTONIA PANTOJA PREPARATORY ACADEMY, A COLLEGE BOARD SCHOOL",
        "num_of_sat_test_takers": "s",
        "sat_critical_reading_avg_score": "s",
        "sat_math_avg_score": "s",
        "sat_writing_avg_score": "s"
    }
]
```

Here's a screen video below in which the School description and SAT Score is viewed.

To checkout the screen video, click on the **SATScoreView.mp4** video found in this repo.

<br />

## Unit Tests
Since I went with an MVVM architecture, I created unit tests for the following:
- View Models (SchoolListViewModel, SATScoreViewModel)
- API Module (Created a mock of `NYCSchoolAPI` **without** any network calls)

Tests included positive, negative, and potential edge cases. Here are some key stats:
- **Total Test Cases**: 11
- **Code Coverage**: 60.3%
- **Total Duration**: 1.05 seconds

Code coverage is pretty impressive given that only views models and the API module were tested.

## If Given More Time, What would I change?
If I was given more time for this coding challenge, I would do the following:
- Use Core Data with `NSFetchedResultsController` to store NYC schools on-disk to reduce memory footprint.
- Utilize a **Coordinator** to handle navigation flow and completely de-couple View Controller/Views from one another.
