//
//  SchoolListViewModel.swift
//  NYCSchools
//
//  Created by Ismail Elmaliki on 4/1/23.
//

import Combine

protocol SchoolListViewModel {
	/// List of NYC schools corresponding to NYC borough.
	var schoolList: [Borough: [School]] { get }
	
	/// NYC School API Client.
	var apiClient: any NYCSchoolAPI { get }
	
	/// Current API Request state of fetching schools from borough.
	var isFetchingSchools: Bool { get }
	
	/// DI to pass APIClient into View Model.
	init(_ apiClient: any NYCSchoolAPI)
	
	/// Fetch schools within specified NYC borough, with completion provided to apply UI changes.
	func getSchools(in borough: Borough, completion: @escaping () -> ())
}
