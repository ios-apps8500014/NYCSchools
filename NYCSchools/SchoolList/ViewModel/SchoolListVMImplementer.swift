//
//  SchoolListVMImplementer.swift
//  NYCSchools
//
//  Created by Ismail Elmaliki on 4/1/23.
//

import Foundation

final class SchoolListVMImplementer: SchoolListViewModel {
	private(set) var schoolList: [Borough: [School]] = [:]
	private(set) var isFetchingSchools = false
	
	let apiClient: NYCSchoolAPI
	
	init(_ apiClient: NYCSchoolAPI) {
		self.apiClient = apiClient
	}
	
	func getSchools(in borough: Borough, completion: @escaping () -> ()) {
		/// Prevent endpoint from being called twice.
		///
		/// To prevent redundant network calls, check:
		/// 1. If list of schools already exist for borough
		/// 2. If API request is currently fetching schools for borough. 
		guard schoolList[borough] == nil && !isFetchingSchools else {
			return
		}
		
		isFetchingSchools = true
		apiClient.fetchSchools(borough) { [weak self] schools, _ in
			guard let self, let schools else { return }
			self.schoolList[borough] = schools
			self.isFetchingSchools = false
			completion()
		}
	}
}
