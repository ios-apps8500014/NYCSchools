//
//  SchoolListController.swift
//  NYCSchools
//
//  Created by Ismail Elmaliki on 3/31/23.
//

import SwiftUI
import UIKit

/// School List `UIViewController` responsible for displaying list of NYC schools in each borough.
final class SchoolListController<T: SchoolListViewModel>: UIViewController, UITableViewDataSource, UITableViewDelegate {
	private lazy var tableView: UITableView = {
		let tbleView = UITableView()
		tbleView.translatesAutoresizingMaskIntoConstraints = false
		return tbleView
	}()
	
	private let viewModel: T
	
	init(viewModel: T) {
		self.viewModel = viewModel
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		/// Fetch schools from Bronx
		fetchSchools(.bronx)
		
		/// Setup tableview
		tableView.register(SchoolTableViewCell.self, forCellReuseIdentifier: SchoolTableViewCell.id)
		tableView.delegate = self
		tableView.dataSource = self
		
		/// Setup view and auto-layout
		view.addSubview(tableView)
		NSLayoutConstraint.activate([
			tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
			tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
			tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
			tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
		])
	}
	
	/// Fetch school list for specific borough via view model then update UI to reflect data changes.
	private func fetchSchools(_ borough: Borough) {
		viewModel.getSchools(in: borough) { [weak self] in
			guard let self = self else {
				return
			}
			
			DispatchQueue.main.async {
				self.tableView.beginUpdates()
				
				/// Insert new borough which will display list of schools.
				/// This approach is much more efficient than `tableView.reloadData()`
				self.tableView.insertSections(IndexSet(integer: borough.rawValue), with: .automatic)
				
				self.tableView.endUpdates()
			}
		}
	}
	
	// MARK: TableView delegates
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return viewModel.schoolList.keys.count
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let borough = Borough(rawValue: section) else {
			return 0
		}
		
		return viewModel.schoolList[borough]?.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		guard let borough = Borough(rawValue: section) else {
			return nil
		}
		
		return borough.title
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let borough = Borough(rawValue: indexPath.section),
			  let school = viewModel.schoolList[borough]?[indexPath.row] else {
			return UITableViewCell()
		}
		
		/// Setup `SchoolTableViewCell` by passing text to its UILabels.
		let cell = tableView.dequeueReusableCell(withIdentifier: SchoolTableViewCell.id, for: indexPath) as! SchoolTableViewCell
		cell.schoolLabel.text = school.name
		cell.addressLabel.text = school.generateSchoolAddress(borough)
		cell.gradesLabel.text = school.gradesText
		
		/// Prevent cell selection from appearing when selected by user.
		cell.selectionStyle = .none
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		guard let borough = Borough(rawValue: indexPath.section),
			  let nextBorough = Borough(rawValue: indexPath.section + 1),
			  let schools = viewModel.schoolList[borough] else {
			return
		}
		
		/// Fetch next list schools for the next borough.
		/// NOTE: Preventing redundant network calls is handled in `getSchools` method of view model.
		if indexPath.row == schools.count - 15 {
			fetchSchools(nextBorough)
		}
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		guard let borough = Borough(rawValue: indexPath.section),
			  let school = viewModel.schoolList[borough]?[indexPath.row] else {
			return
		}
		
		let viewModel = SATScoreVMImplementer(school: school, apiClient: viewModel.apiClient)
		let satScoreVC = UIHostingController(rootView: SATScoreView(viewModel: viewModel))
		
		/// Display `SATScoreView` for specific school.
		navigationController?.pushViewController(satScoreVC, animated: true)
	}
}
