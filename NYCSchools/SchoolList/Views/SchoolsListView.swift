//
//  SchoolsListView.swift
//  NYCSchools
//
//  Created by Ismail Elmaliki on 3/31/23.
//

import SwiftUI

/// SwiftUI View of `SchoolListController`.
struct SchoolsListView: UIViewControllerRepresentable {
	func makeUIViewController(context: Context) -> some UIViewController {
		let viewModel = SchoolListVMImplementer(NYCSchoolProdAPI.shared)
		return SchoolListController(viewModel: viewModel)
	}
	
	func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
		
	}
}
