//
//  SchoolTableViewCell.swift
//  NYCSchools
//
//  Created by Ismail Elmaliki on 4/1/23.
//

import UIKit

/// Custom `UITableViewCell for NYC School.
///
/// The following is presented from this `UITableViewCell` wrapped in a vertical `UIStackView`:
/// 1. School name
/// 2. School address
/// 3. School grades
final class SchoolTableViewCell: UITableViewCell {
	static let id = "schoolTableViewCell"
	private lazy var containerView: UIStackView = {
		let stackView = UIStackView()
		stackView.translatesAutoresizingMaskIntoConstraints = false
		stackView.axis = .vertical
		stackView.alignment = .leading
		stackView.distribution = .equalSpacing
		stackView.spacing = 4
		return stackView
	}()
	
	private(set) lazy var schoolLabel: UILabel = {
		let label = UILabel()
		label.numberOfLines = 0
		label.font = .systemFont(ofSize: 20, weight: .bold)
		label.translatesAutoresizingMaskIntoConstraints = false
		return label
	}()
	
	private(set) lazy var addressLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.numberOfLines = 0
		label.font = .systemFont(ofSize: 16, weight: .regular)
		return label
	}()
	
	private(set) lazy var gradesLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.numberOfLines = 0
		label.font = .systemFont(ofSize: 16, weight: .regular)
		return label
	}()
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupView()
		setupLayout()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	/// Add views to contentView.
	private func setupView() {
		[schoolLabel, addressLabel, gradesLabel].forEach { containerView.addArrangedSubview($0) }
		contentView.addSubview(containerView)
	}
	
	/// Setup auto-layout constraints.
	private func setupLayout() {
		NSLayoutConstraint.activate([
			containerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
			containerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
			containerView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
			containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
		])
	}
}
