//
//  NYCSchoolProdAPI.swift
//  NYCSchools
//
//  Created by Ismail Elmaliki on 4/1/23.
//

import Foundation

final class NYCSchoolProdAPI: NYCSchoolAPI {
	static let shared: NYCSchoolAPI = NYCSchoolProdAPI()
	
	/// Base URL for NYC endpoint.
	private let baseURL: URL
	
	/// Shared URL session to initiate data requests.
	private let urlSession: URLSession
	
	private init() {
		baseURL = URL(string: "https://data.cityofnewyork.us/resource/")!
		urlSession = URLSession.shared
	}
	
	func fetchSchools(_ borough: Borough, completion: @escaping ([School]?, SchoolAPIError?) -> ()) {
		/// School url path.
		///
		/// Parameters being passed include:
		/// - **boro**: Key of NYC borough
		/// - **order**: Sorting list of schools by **school_name**
		let schoolsPath = "s3k6-pzi2.json?boro=\(borough.key)&$order=school_name"
		
		let schoolsURL = URL(string: schoolsPath, relativeTo: baseURL)!
		
		let task = urlSession.dataTask(with: URLRequest(url: schoolsURL)) { data, _, error in
			guard error == nil, let data else {
				/// Print out error that occurred from initializing API request.
				print("Error fetching schools data: \(String(describing: error))")
				completion(nil, SchoolAPIError.schoolListServerError)
				return
			}
			
			do {
				let decoder = JSONDecoder()
				let schools = try decoder.decode([School].self, from: data)
				
				/// Return schools list via completion handler.
				completion(schools, nil)
			} catch {
				/// Print out error that occurred during data decoding.
				print("Error with schools data: \(String(describing: error))")
				completion(nil, SchoolAPIError.schoolListDecodeError)
			}
		}
		
		task.resume()
	}
	
	func fetchSATScore(_ schoolId: String, completion: @escaping (SATScore?, SchoolAPIError?) -> ()) {
		/// SAT Score URL path.
		///
		/// Parameter being passed includes:
		/// - **dbn**: Unique ID of NYC school.
		let satScorePath = "f9bf-2cp4.json?dbn=\(schoolId)"
		
		let satScoreURL = URL(string: satScorePath, relativeTo: baseURL)!
		
		let task = urlSession.dataTask(with: URLRequest(url: satScoreURL)) { data, _, error in
			guard error == nil, let data else {
				/// Print out error that occurred from initializing API request.
				print("Error fetching SAT Score: \(String(describing: error))")
				completion(nil, SchoolAPIError.satScoreServerError)
				return
			}
			
			do {
				let decoder = JSONDecoder()
				let score = try decoder.decode([SATScore].self, from: data)
				
				/// Return SAT Score.
				completion(score.first, nil)
			} catch {
				/// Print out error that occurred during data decoding.
				print("Error decoding SAT Score: \(String(describing: error))")
				completion(nil, SchoolAPIError.satScoreDecodeError)
			}
		}
		task.resume()
	}
}
