//
//  SchoolAPIError.swift
//  NYCSchools
//
//  Created by Ismail Elmaliki on 4/2/23.
//

import Foundation

enum SchoolAPIError: Error {
	case schoolListServerError
	case schoolListDecodeError
	case satScoreServerError
	case satScoreDecodeError
}
