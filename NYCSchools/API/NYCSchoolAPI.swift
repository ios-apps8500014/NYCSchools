//
//  NYCSchoolAPI.swift
//  NYCSchools
//
//  Created by Ismail Elmaliki on 3/31/23.
//

import Foundation

/// NYC School API blueprint.
///
/// Abstraction of API client to create:
/// - Production version of API called **NYCSchoolProdAPI**
/// - Mock version of API for unit testing purposes
protocol NYCSchoolAPI {
	/// Singleton shared instance of API.
	static var shared: any NYCSchoolAPI { get }
	
	/// Fetch list of schools for NYC borough.
	func fetchSchools(_ borough: Borough, completion: @escaping ([School]?, SchoolAPIError?) -> ())
	
	/// Fetch SAT Score for specific NYC school.
	///
	/// Requires school's unique ID (**dbn**) to fetch school's SAT Scores.
	func fetchSATScore(_ schoolId: String, completion: @escaping (SATScore?, SchoolAPIError?) -> ())
	
	/// Trigger School API failure for unit testing purposes
	func triggerFailure(with error: SchoolAPIError?)
}

extension NYCSchoolAPI {
	func triggerFailure(with error: SchoolAPIError?) {}
}
