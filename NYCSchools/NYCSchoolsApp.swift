//
//  NYCSchoolsApp.swift
//  NYCSchools
//
//  Created by Ismail Elmaliki on 3/31/23.
//

import SwiftUI

@main
struct NYCSchoolsApp: App {
	let navigationTitle = "NYC Schools"
	
    var body: some Scene {
        WindowGroup {
			NavigationView {
				SchoolsListView()
					.navigationTitle(navigationTitle)
			}
        }
    }
}
