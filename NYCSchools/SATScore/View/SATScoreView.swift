//
//  SATScoreView.swift
//  NYCSchools
//
//  Created by Ismail Elmaliki on 4/1/23.
//

import SwiftUI

/// SAT Score View - SwiftUI View to display school's name, overview, and SAT Scores.
///
/// SAT Scores are displayed conditionally - depending on whether or not they exist for a school.
struct SATScoreView<T: SATScoreViewModel>: View {
	@StateObject var viewModel: T
	
    var body: some View {
		/// Utilize `ScrollView` in case school's summary is too long.
		ScrollView {
			/// Lazy load VStack
			LazyVStack(alignment: .leading, spacing: 15) {
				/// School name
				Text(viewModel.school.name)
					.font(.system(size: 20, weight: .bold))
				
				/// School overview
				Text(viewModel.school.overview)
					.font(.system(size: 16, weight: .regular))
				
				/// Conditionally display SAT Scores if available
				if viewModel.isSATScoreFetched && viewModel.isSATScoreAvailable,
				   let satScore = viewModel.school.satScore {
					/// SAT Scores title
					Text("SAT Average Scores")
						.font(.system(size: 20, weight: .bold))
					
					/// Reading scores
					Text("Reading:\t \(satScore.averageReadingScore)")
						.font(.system(size: 16, weight: .regular))
					
					/// Math scores
					Text("Math:\t\t \(satScore.averageMathScore)")
						.font(.system(size: 16, weight: .regular))
					
					/// Writing scores
					Text("Writing:\t \(satScore.averageWritingScore)")
						.font(.system(size: 16, weight: .regular))
				}
			}
			.padding()
		}
		.onAppear {
			/// Fetch SAT Scores.
			viewModel.getSATScore{ }
		}
    }
}
