//
//  SATScoreViewModel.swift
//  NYCSchools
//
//  Created by Ismail Elmaliki on 4/1/23.
//

import Foundation

/// SAT Score View Model.
///
/// Responsible for facilitating communication between `NYCSchoolAPI` and `SATScoreView`.
protocol SATScoreViewModel: ObservableObject {
	/// Checks if SAT Score has been fetched.
	///
	/// UI is alerted when this value changes, prompting UI to update.
	var isSATScoreFetched: Bool { get }
	
	/// NYC School.
	///
	/// Its property `satScore` is updated once it's fetched from API. To prevents redudant network calls from being executed.
	var school: School { get }
	var apiClient: any NYCSchoolAPI { get }
	
	/// Determines if SAT Score can be displayed to UI.
	var isSATScoreAvailable: Bool { get }
	
	/// Initializer to support Dependency Injection.
	init(school: School, apiClient: NYCSchoolAPI)
	
	
	/// Fetches SAT Score for school.
	///
	/// If SAT Score already exists, then no network call is requested. Otherwise a network call is requested to fetch SAT Score.
	func getSATScore(completion: @escaping () -> ())
}

extension SATScoreViewModel {
	var isSATScoreAvailable: Bool {
		guard let satScore = school.satScore,
			  !satScore.averageMathScore.isEmpty else {
			return false
		}
		
		return true
	}
}
