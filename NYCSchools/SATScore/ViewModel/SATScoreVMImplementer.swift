//
//  SATScoreVMImplementer.swift
//  NYCSchools
//
//  Created by Ismail Elmaliki on 4/1/23.
//

import Combine
import Foundation

/// Production version of SATScore View Model.
///
/// This view model is responsible for performing actual network requests if necessary to fetch SAT Score.
final class SATScoreVMImplementer: SATScoreViewModel {
	@Published private(set) var isSATScoreFetched = false
	let school: School
	let apiClient: NYCSchoolAPI
	
	init(school: School, apiClient: NYCSchoolAPI) {
		self.school = school
		self.apiClient = apiClient
	}
	
	func getSATScore(completion: @escaping () -> ()) {
		guard school.satScore == nil else {
			isSATScoreFetched = true
			return
		}
		
		apiClient.fetchSATScore(school.id) { [weak self] satScore, error in
			guard let self, error == nil else {
				return
			}
			
			DispatchQueue.main.async {
				self.school.satScore = satScore ?? SATScore(id: self.school.id)
				self.isSATScoreFetched = true
				completion()
			}
		}
	}
}

