//
//  Borough.swift
//  NYCSchools
//
//  Created by Ismail Elmaliki on 3/31/23.
//

import Foundation

/// List of unique NYC Boroughs.
///
/// Helps display NYC school data borough by borough.
enum Borough: Int, CaseIterable {
	case bronx = 0
	case brooklyn
	case manhattan
	case queens
	case statenIsland
	
	/// Get borough based on `key` that's provided.
	static func getBorough(with key: String) -> Borough? {
		switch key {
		case "K": return .brooklyn
		case "Q": return .queens
		case "M": return .manhattan
		case "R": return .statenIsland
		case "X": return .bronx
		default: return nil
		}
	}
	
	/// Key that represents current Borough.
	///
	/// The following keys are represented as follows:
	/// - **X** = Bronx
	/// - **K** = Brooklyn
	/// - **M** = Manhattan
	/// - **Q** = Queens
	/// - **R** = Staten Island
	var key: String {
		switch self {
		case .bronx:
			return "X"
		case .brooklyn:
			return "K"
		case .manhattan:
			return "M"
		case .queens:
			return "Q"
		case .statenIsland:
			return "R"
		}
	}
	
	/// Full borough name.
	///
	/// Allows borough name to be presented via UI.
	var title: String {
		switch self {
		case .brooklyn:
			return NSLocalizedString("Brooklyn", comment: "bklyn")
		case .manhattan:
			return NSLocalizedString("Manhattan", comment: "m")
		case .queens:
			return NSLocalizedString("Queens", comment: "q")
		case .statenIsland:
			return NSLocalizedString("Staten Island", comment: "si")
		case .bronx:
			return NSLocalizedString("Bronx", comment: "brx")
		}
	}
}
