//
//  School.swift
//  NYCSchools
//
//  Created by Ismail Elmaliki on 3/31/23.
//

import Foundation

/// NYC School model
///
/// It includes selected attributes such as borough, id, name, grades, and overview.
/// The `satScore` is optional initially until user views additional information regarding NYC school.
final class School: Codable {
	
	/// Represents NYC borough such as "K" for Brooklyn, "M" for Manhattan.
	let borough: String
	
	/// Unique `dbn` id of school.
	let id: String
	
	/// Name of school.
	let name: String
	
	/// Grades supported by school, whether it's elementary (PreK-5), middle school (6-8), or high school (9-12).
	let grades: String
	
	/// Summary of the school and its mission.
	let overview: String
	
	/// Address of school, excluding city and zip code.
	let address: String
	
	/// Zip code of school
	let zipCode: String
	
	/// Sat Score for school
	var satScore: SATScore?
	
	enum SchoolKey: CodingKey {
		case boro
		case dbn
		case school_name
		case overview_paragraph
		case finalgrades
		case primary_address_line_1
		case zip
	}
	
	/// Decodes School backend data into client `School`
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: SchoolKey.self)
		borough = try values.decode(String.self, forKey: .boro)
		id = try values.decode(String.self, forKey: .dbn)
		name = try values.decode(String.self, forKey: .school_name)
		grades = try values.decode(String.self, forKey: .finalgrades)
		overview = try values.decode(String.self, forKey: .overview_paragraph)
		address = try values.decode(String.self, forKey: .primary_address_line_1)
		zipCode = try values.decode(String.self, forKey: .zip)
	}
	
	#if DEBUG
	/// For unit testing purposes only to create mock School data.
	init(
		borough: String = Borough.allCases.randomElement()!.key,
		id: String = UUID().uuidString,
		name: String = ["Brooklyn Technical HS", "Stuyvesant", "Staten Island Technical HS", "Bronx Science High School", "Townsend Harris"].randomElement()!,
		grades: String = ["6-12", "9-12"].randomElement()!,
		overview: String,
		address: String = ["125-23 Gates Street", "123-34 5th Avenue"].randomElement()!,
		zipCode: String = ["11253", "10012"].randomElement()!
	) {
		self.borough = borough
		self.id = id
		self.name = name
		self.grades = grades
		self.overview = overview
		self.address = address
		self.zipCode = zipCode
	}
	#endif
	
	/// Generates grades supported into presentable UI text.
	var gradesText: String {
		return NSLocalizedString("Grades: \(grades)", comment: "grades")
	}
	
	/// Generates school address into spresentable UI text.
	func generateSchoolAddress(_ borough: Borough) -> String {
		return "\(address), \(borough.title), NY \(zipCode)"
	}
}
