//
//  SATScore.swift
//  NYCSchools
//
//  Created by Ismail Elmaliki on 3/31/23.
//

import Foundation

/// SAT Score for NYC school.
///
/// Scores include reading, math, and writing.
final class SATScore: Codable {
	/// Unique `dbn` id of school.
	let id: String
	
	/// Average reading score.
	var averageReadingScore: String
	
	/// Average math score.
	var averageMathScore: String
	
	/// Average writing score.
	var averageWritingScore: String
	
	enum SATKey: CodingKey {
		case dbn
		case sat_critical_reading_avg_score
		case sat_math_avg_score
		case sat_writing_avg_score
	}
	
	/// Decodes SATScore backend data into client `SATScore`
	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: SATKey.self)
		self.id = try container.decode(String.self, forKey: .dbn)
		self.averageReadingScore = try container.decode(String.self, forKey: .sat_critical_reading_avg_score)
		self.averageMathScore = try container.decode(String.self, forKey: .sat_math_avg_score)
		self.averageWritingScore = try container.decode(String.self, forKey: .sat_writing_avg_score)
		
		averageMathScore = scoreTransformer(averageMathScore)
		averageReadingScore = scoreTransformer(averageReadingScore)
		averageWritingScore = scoreTransformer(averageWritingScore)
	}
	
	#if DEBUG
	/// For unit testing purposes only to create mock SATScore data
	init(
		id: String,
		averageMathScore: String,
		averageReadingScore: String,
		averageWritingScore: String
	) {
		self.id = id
		self.averageMathScore = averageMathScore
		self.averageReadingScore = averageReadingScore
		self.averageWritingScore = averageWritingScore
	}
	#endif
	
	/// Verifies SAT Scores are only digits. Otherwise score will become an empty string.
	///
	/// Occassionally the NYC SAT Score endpoint will return SAT Scores with a value `s`.
	/// To prevent scores from being in UI, scores will be emptied out.
	///
	/// An example of an invalid SAT score is with the school ID dbn/id `08X376`.
	func scoreTransformer(_ score: String) -> String {
		for char in score where !char.isNumber {
			return ""
		}
		
		return score
	}
	
	/// Initializer for with passing in school id.
	///
	/// There are some schools without an SAT Score within the NYC school database.
	init(id: String) {
		self.id = id
		averageMathScore = ""
		averageReadingScore = ""
		averageWritingScore = ""
	}
}
