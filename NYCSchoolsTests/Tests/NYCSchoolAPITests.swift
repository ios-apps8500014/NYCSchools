//
//  NYCSchoolAPITests.swift
//  NYCSchoolsTests
//
//  Created by Ismail Elmaliki on 4/2/23.
//

@testable import NYCSchools
import XCTest

/// Unit tests for **NYCSchoolAPI** via **NYCSchoolMockAPI** to fake network calls.
final class NYCSchoolAPITests: XCTestCase {
	private var sut: NYCSchoolAPI!
	
	override func setUp() {
		super.setUp()
		/// Use NYCMockAPI
		sut = NYCSchoolMockAPI.shared
		
		/// Clear any errors that were set for testing purposes
		sut.triggerFailure(with: nil)
	}
	
	override func tearDown() {
		sut = nil
		super.tearDown()
	}
	
	func test_trigger_schoolListServerError() {
		let expectation = expectation(description: "school-list-server-error")
		triggerSchoolAPIError(expectation, .schoolListServerError)
	}
	
	func test_trigger_schoolListDecodeError() {
		let expectation = expectation(description: "school-list-decode-error")
		triggerSchoolAPIError(expectation, .schoolListDecodeError)
	}
	
	func test_trigger_satScoreDecodeError() {
		let expectation = expectation(description: "sat-score-server-error")
		triggerSchoolAPIError(expectation, .satScoreDecodeError)
	}
	
	func test_trigger_satScoreServerError() {
		let expectation = expectation(description: "sat-score-server-error")
		triggerSchoolAPIError(expectation, .satScoreServerError)
	}
	
	/// Common function to trigger unit testing for API Errors.
	private func triggerSchoolAPIError(_ expectation: XCTestExpectation, _ error: SchoolAPIError) {
		let expectedError = error
		var actualError: SchoolAPIError?
		
		sut.triggerFailure(with: expectedError)
		
		sut.fetchSchools(.brooklyn) { _, error in
			actualError = error
			expectation.fulfill()
		}
			
		wait(for: [expectation], timeout: 0.5)
		
		XCTAssertEqual(expectedError, actualError)
	}
	
	func test_fetchSchoolsList() {
		let expectation = expectation(description: "fetch-schools-list")
		var schoolsList: [School] = []
		
		sut.fetchSchools(.bronx) { schools, _ in
			guard let schools else {
				XCTFail("Schools are not returned")
				return
			}
			
			schoolsList = schools
			expectation.fulfill()
		}
		
		wait(for: [expectation], timeout: 0.5)
		
		XCTAssertFalse(schoolsList.isEmpty)
		
		/// Verify each borough key for school is `bronx`
		schoolsList.forEach { XCTAssertEqual(Borough.bronx.key, $0.borough) }
	}
	
	func test_fetchSatScore() {
		let expectation = expectation(description: "fetch-SAT-score")
		let schoolID = UUID().uuidString
		var currentSatScore: SATScore?
		
		sut.fetchSATScore(schoolID) { satScore, _ in
			currentSatScore = satScore
			expectation.fulfill()
		}
		
		wait(for: [expectation], timeout: 0.5)
		
		XCTAssertEqual(schoolID, currentSatScore?.id)
	}
}
