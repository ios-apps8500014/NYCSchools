//
//  SATScoreVMTests.swift
//  NYCSchoolsTests
//
//  Created by Ismail Elmaliki on 4/2/23.
//

@testable import NYCSchools
import XCTest

/// Unit tests for **SATScoreViewModel*
final class SATScoreVMTests: XCTestCase {
	var sut: (any SATScoreViewModel)!
	
	override func setUp() {
		super.setUp()
		sut = SATScoreVMImplementer(
			school: School(overview: String.randomString(length: 20)),
			apiClient: NYCSchoolMockAPI.shared
		)
	}
	
	override func tearDown() {
		sut = nil
		super.tearDown()
	}
	
	func test_getSATScore_whenItAlreadyExists() {
		let schoolID = sut.school.id
		sut.school.satScore = SATScore(id: schoolID)
		
		let expectation = expectation(description: "get-already-existing-SAT-score")
		
		/// Expect expectation to fail since sat score already exists and completion handler is not called
		expectation.isInverted = true
		sut.getSATScore {
			expectation.fulfill()
		}
		
		wait(for: [expectation], timeout: 0.5)
		
		XCTAssertNotNil(sut.school.satScore)
	}
	
	func test_getSATScore_andCheckID() {
		let expectation = expectation(description: "get-sat-score")
		let expectedSchoolID = sut.school.id
		
		sut.getSATScore {
			expectation.fulfill()
		}
		
		wait(for: [expectation], timeout: 0.75)
		
		XCTAssertNotNil(sut.school.satScore)
		XCTAssertEqual(sut.school.satScore?.id, expectedSchoolID)
	}
}
