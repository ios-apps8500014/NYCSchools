//
//  SchoolListVMTests.swift
//  NYCSchoolsTests
//
//  Created by Ismail Elmaliki on 4/2/23.
//

@testable import NYCSchools
import XCTest

/// Unit tests for **SchoolListViewModel**
final class SchoolListVMTests: XCTestCase {
	var sut: SchoolListViewModel!
	
	override func setUp() {
		super.setUp()
		sut = SchoolListVMImplementer(NYCSchoolMockAPI.shared)
	}
	
	override func tearDown() {
		sut = nil
		super.tearDown()
	}
	
	func test_getSchools_fromRandomBorough() {
		let randomBorough = Borough.allCases.randomElement()!
		let expectation = expectation(description: "get-schools-random-borough")
		
		sut.getSchools(in: randomBorough) {
			expectation.fulfill()
		}
		
		wait(for: [expectation], timeout: 0.5)
		
		XCTAssertNotNil(sut.schoolList[randomBorough])
	}
	
	func test_getSchools_fromSameBoroughTwice() {
		let randomBorough = Borough.allCases.randomElement()!
		let expectationOne = expectation(description: "get-schools-same-borough-1")
		
		sut.getSchools(in: randomBorough) {
			expectationOne.fulfill()
		}
		wait(for: [expectationOne], timeout: 0.5)
		
		let expectedSchoolList = sut.schoolList
		let expectationTwo = expectation(description: "get-schools-same-borough-2")
		/// expect this expectation to not be called
		expectationTwo.isInverted = true
		/// Expectation not needed since function will return
		sut.getSchools(in: randomBorough) {
			expectationTwo.fulfill()
		}
		
		wait(for: [expectationTwo], timeout: 0.5)
		
		XCTAssertEqual(sut.schoolList, expectedSchoolList)
	}
	
	func test_getSchools_fromTwoDifferentBoroughs() {
		let firstBorough = Borough.allCases.randomElement()!
		var secondBorough = Borough.allCases.randomElement()!
		
		/// Prevent second borough from being equal to first
		repeat {
			secondBorough = Borough.allCases.randomElement()!
		} while firstBorough == secondBorough
		
		let firstExpectation = expectation(description: "fetch-from-borough-1")
		sut.getSchools(in: firstBorough) {
			firstExpectation.fulfill()
		}
		wait(for: [firstExpectation], timeout: 0.5)
		
		let secondExpectation = expectation(description: "fetch-from-borough-2")
		sut.getSchools(in: secondBorough) {
			secondExpectation.fulfill()
		}
		wait(for: [secondExpectation], timeout: 0.5)
		
		XCTAssertNotNil(sut.schoolList[firstBorough])
		XCTAssertNotNil(sut.schoolList[secondBorough])
	}
}
