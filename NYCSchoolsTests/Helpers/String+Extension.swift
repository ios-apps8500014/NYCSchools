//
//  String+Extension.swift
//  NYCSchoolsTests
//
//  Created by Ismail Elmaliki on 4/2/23.
//

import Foundation

extension String {
	/// Generate a random length string
	static func randomString(length: Int) -> String {
		let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
		return String((0..<length).map{ _ in letters.randomElement()! })
	}
}
