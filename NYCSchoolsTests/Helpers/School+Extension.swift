//
//  School+Extension.swift
//  NYCSchoolsTests
//
//  Created by Ismail Elmaliki on 4/2/23.
//

@testable import NYCSchools
import Foundation

extension School: Equatable {
	public static func == (lhs: School, rhs: School) -> Bool {
		return lhs.id == rhs.id
	}
}
