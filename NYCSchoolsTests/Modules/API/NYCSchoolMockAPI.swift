//
//  NYCSchoolMockAPI.swift
//  NYCSchoolsTests
//
//  Created by Ismail Elmaliki on 4/2/23.
//

@testable import NYCSchools
import Foundation

/// Mock version of NYC School API for unit testing purposes.
final class NYCSchoolMockAPI: NYCSchoolAPI {
	static let shared: NYCSchoolAPI = NYCSchoolMockAPI()
	
	private init() {}
	
	/// Specific error to pass for unit testing purposes
	private var triggeredError: SchoolAPIError?
	
	func triggerFailure(with error: SchoolAPIError?) {
		triggeredError = error
	}
	
	func fetchSchools(_ borough: NYCSchools.Borough, completion: @escaping ([School]?, SchoolAPIError?) -> ()) {
		guard triggeredError == nil else {
			completion(nil, triggeredError)
			return
		}
		
		/// Generate list of `School` models for unit testing purposes if there are no errors.
		var schools: [School] = []
		for _ in 0..<10 {
			schools.append(School(borough: borough.key, overview: String.randomString(length: 20)))
		}
		
		completion(schools, nil)
	}
	
	func fetchSATScore(_ schoolId: String, completion: @escaping (SATScore?, SchoolAPIError?) -> ()) {
		guard triggeredError == nil else {
			completion(nil, triggeredError)
			return
		}
		
		let scoreRange = 400...800
		
		/// Generate SAT Score score assuming there are no errors being triggered.
		let satScore = SATScore(
			id: schoolId,
			averageMathScore: "\(Int.random(in: scoreRange))",
			averageReadingScore: "\(Int.random(in: scoreRange))",
			averageWritingScore: "\(Int.random(in: scoreRange))"
		)
		
		completion(satScore, nil)
	}
}
